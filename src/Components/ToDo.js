import React, { Component } from 'react';
import ToDoAdd from './ToDoAdd.js'

class ToDo extends Component {

  removeCurrentItem(event) {    
     this.props.removeToDo(this.props.itemIndex)
  }

 hideCurrentItem(event) {
    console.log('this.props ');
    // console.log('this.props ', this.props);
     this.props.hideCurrentItem(this.props.itemIndex,event.target.checked)
  }

  render() {
    let {todoString } = this.props
    console.log('ToDo will render', todoString);
    // var todoString = props.todoString
    
    //var isShow = this.props.isShow ? 'block' : 'none';
    
    return (            
      //, display: isShow

      <div style={{ padding: '3px'}}>
        <input checked={this.props.isChecked} type="checkbox" name='{todoString}' onChange={this.hideCurrentItem.bind(this)} />
          {todoString}        
        <input type="button"  defaultValue="X" onClick={this.removeCurrentItem.bind(this)}/>
      </div>
    )
  }
}

export default ToDo;