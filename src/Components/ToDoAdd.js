import React, { Component } from 'react';

class ToDoAdd extends Component {
	constructor(props) {
		super(props)
		this.state = {
			toDoTxtVal: '',
			//showAllCheckedList: false
		}
	}

	textValueChangedHandler(e) {
		this.setState({
			toDoTxtVal: e.target.value
		})
	}

	addButtonHandler() {
		// sad
		if (this.state.toDoTxtVal != '') {
			this.props.addToDoCallback(this.state.toDoTxtVal)
			var empty = '';
			this.setState({
				toDoTxtVal: empty,
			})
		}
		else {
			alert('Please enter vale to add');
		}
	}

	showAllCompletedTodo(e) {
		// this.setState(
		// 	{
		// 		showAllCheckedList: e.target.checked
		// 	}
		// );

		this.props.showAllCompletedTodo(e.target.checked);
	}

	render() {
		console.log('ToDoADDContainer rendered');

		let {addToDoCallback} = this.props
		return (
			<div>

				<div>
					Show All Checked List
            &nbsp;&nbsp;&nbsp;&nbsp;
						{/*defaultValue={this.props.showAll}*/}
					<input type="checkbox" name="showAllCheckList" checked={this.props.showAll} onChange={this.showAllCompletedTodo.bind(this)} />
				</div>
				<br />
				<input type='Button' defaultValue='Add' onClick={this.addButtonHandler.bind(this)} />
				<input type='text' value={this.state.toDoTxtVal} onChange={this.textValueChangedHandler.bind(this)} />
				<br />
			</div>
		)
	}
}

export default ToDoAdd;