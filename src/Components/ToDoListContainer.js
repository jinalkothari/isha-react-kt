import React, { Component } from 'react';
import ToDo from './ToDo.js'
import ToDoAdd from './ToDoAdd.js'

class ToDoListContainer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      todoList: [
        {
          "toDoName": 'Buy milk',
          "isChecked": false
        }, {
          "toDoName": 'Wash clothes',
          "isChecked": false
        },
        {
          "toDoName": 'Do something else',
          "isChecked": false
        },
        {
          "toDoName": 'c',
          "isChecked": false
        }],

      name: 'Jinal',
      showAll: false
    }

    // this.todoList = ['Buy milk', 'Wash clothes']
  }

  addToDoCallback(todoString) {
    var x = [{
      "toDoName": todoString,
      "isChecked": false
    }];

    this.setState(
      {
        todoList: this.state.todoList.concat(x)
      }
    )
  }

  removeToDo(itemIndex) {
    let {todoList} = this.state
    // This part is looking difficult
    //var index = todoList.indexOf(doStr);
    let fisrtHalf = todoList.slice(0, itemIndex);
    let secondHalf = todoList.slice(itemIndex + 1);
    let newTodoList = fisrtHalf.concat(secondHalf)
    this.setState(
      {
        todoList: newTodoList,
      }
    )

    // let new_state = {
    //   x : state.x,
    //   y: state.y,
    //   todoList: newTodoList
    // }      
    // state = new_state

  }

  hideCurrentItem(itemIndex, isChecked) {
    let {todoList} = this.state

    var newlst = [];
    newlst = newlst.concat(this.state.todoList);
    //newlst[itemIndex].isShow = false;
    newlst[itemIndex].isChecked = isChecked;

    this.setState(
      {
        todoList: newlst
      }
    )
  }

  showAllCompletedTodo(isShowAll) {
    // console.log('isShowAll ', isShowAll);

    // var newCollection = [];
    // newCollection = newCollection.concat(this.state.todoList)

    // if (isShowAll) {
    //   newCollection.forEach(function (item, index) {
    //     if (item.isChecked)
    //       item.isShow = true;
    //     else
    //       item.isShow = false;
    //   });
    // }

    // else {
    //   newCollection.forEach(function (item, index) {
    //     if (item.isChecked)
    //       item.isShow = false;
    //     else
    //       item.isShow = true;
    //   });
    // }

    // this.setState({
    //   todoList: newCollection
    // });

    this.setState({
      showAll: isShowAll
    });
  }

  render() {
    console.log('To do list container rendered');

    let {todoList, } = this.state
    // var todoList = this.state.todoList
    let todoListDisplay = []
    for (var i = 0; i < todoList.length; i++) {

      var a = (
        <div key={i}>
          <ToDo hideCurrentItem={this.hideCurrentItem.bind(this)} removeToDo={this.removeToDo.bind(this)} todoString={todoList[i].toDoName} isChecked={todoList[i].isChecked} itemIndex={i} />
        </div>
      )

      if (!todoList[i].isChecked || this.state.showAll)
        todoListDisplay.push(a);
    }

    var b = (
      <div>
        <ToDoAdd showAllCompletedTodo={this.showAllCompletedTodo.bind(this)} addToDoCallback={this.addToDoCallback.bind(this)} showAll={this.state.showAll} />
        {todoListDisplay}
        {2 === 3}
      </div>
    );
    return b
  }
}

export default ToDoListContainer;