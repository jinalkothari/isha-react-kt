import React from 'react';
import { PhotoStory, VideoStory } from './stories';

// const components =
//     {
//         photo: PhotoStory,
//         video: VideoStory
//     };

// function NumberDescriber(props) {
//     let description;

//     if (props.number % 2 == 0) {
//         description = <strong> even </strong>;
//     } else {
//         description = <i> odd </i>;
//     }

//     return <div> {props.number} is an {description} number </div>;
// }

// function App1() {
//     return <Greeting firstName="Ben" lastName="Hector" />;
// }

// function App2() {
//     const props = {
//         firstName: 'Ben',
//         lastName: 'Hector'
//     };

//     return <Greeting {...props } />;
// }

// function Story(props) {
//     // Correct! JSX type can be a capitalized variable.
//     const SpecificStory = components[props.storyType];
//     return < SpecificStory story={props.story} />;
// }

function Item(props) {
    return <li> {props.message} </li>;
}

function TodoList() {
    const todos = ['finish doc', 'submit pr', 'nag dan to review'];

    return (
        <ul>
            {
                todos.map((message) =>
                    <Item key={message} message={message} />)
            }
        </ul>
    );
}



function ListOfTenThings() {
  return (
    <Repeat numTimes={10}>
      {(index) => <div key={index}>This is item {index} in the list</div>}
    </Repeat>
  );
}
